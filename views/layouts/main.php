<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\widget\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script type="application/javascript" src="/admin/js/jquery-2.1.1.js"></script>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="wrapper">
    <?php
    if (!isset($_REQUEST['hide_menu'])){
    ?>
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" class="img-circle" src="<?= Yii::$app->homeUrl ?>/img/profile_small.jpg"/>
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold"><?= Yii::$app->user->identity->name ?></strong>
                                </span>
                                <span class="text-muted text-xs block"><?= \backend\models\User::$role[Yii::$app->user->identity->role] ?> <b class="caret"></b></span>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <form action="/admin/logout" id="logout" method="post">
                                <? echo Html :: hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
                            </form>
                            <li><a href="javascript:;" onclick="$('#logout').submit();">Выход</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        W4U
                    </div>
                </li>
                <div class="input-group-prepend" style="position: relative">
                    <button tabindex="-1" class="btn btn-white" type="button" style="width: 80%;">Добавить</button>
                    <button data-toggle="dropdown" class="btn btn-white dropdown-toggle" style="position: relative;width: 40px;left: -4px;" type="button"><i class="fa fa-chevron-down"></i></button>
                    <ul class="dropdown-menu" style="width: 100%;">
                        <li><a data-toggle="modal" href="#add-item">Объект</a></li>
                        <li><a data-toggle="modal" href="#addContact">Контакт</a></li>
                        <li><a href="/admin/tasks/add">Задачу</a></li>
                    </ul>
                </div>
                <?php
                echo Menu::widget(['active'=>$_SERVER['REQUEST_URI']]);
                ?>
            </ul>

        </div>
    </nav>
    <? } ?>
    <div id="page-wrapper" <? if (isset($_REQUEST['hide_menu'])) echo 'style="margin-left:0!important;"'; ?> class="gray-bg dashbard-1">
<!--        <div class="row border-bottom">-->
<!--            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">-->
<!--                <div class="navbar-header">-->
<!--                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>-->
<!--                    </a>-->
<!--                    <form role="search" class="navbar-form-custom" action="search_results.html">-->
<!--                        <div class="form-group">-->
<!--                            <div class="col-md-5"><input type="text" placeholder="Пошук..." class="form-control" name="top-search" id="top-search"></div>-->
<!--                            <div class="col-md-5">-->
<!--                                <select class="form-control">-->
<!--                                    <option value="">розділ</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <div class="col-md-2">-->
<!--                                <button class="btn btn-success btn-sm" style="margin-top: 15px;"><i class="fa fa-search"></i></button>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </nav>-->
<!--        </div>-->
        <?= $content ?>
    </div>
    <? //\backend\widget\Chat::widget(); ?>
    <? //\backend\widget\Sidebar::widget(); ?>
</div>
<!-- MODAL -->
<div class="modal inmodal fade" id="edit-item" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form id="save-item-prop" class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Радектирование объявления</h4>
            </div>
            <div class="modal-body">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Отмена</button>
                <button type="button" onclick="item.save();" class="btn btn-primary">Сохранить</button>
            </div>
        </form>
    </div>
</div>

<div class="modal inmodal fade" id="add-item" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добавить объект</h4>
            </div>
            <div class="modal-body">
                <?php
                $modelItems = new \backend\models\Items();
                $form = \yii\bootstrap\ActiveForm::begin(['action'=>'/admin/site/save-items', 'options'=>['data-pjax' => true]]);
                ?>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Объект</a></li>
                    <li role="presentation"><a href="#contact" aria-controls="profile" role="tab" data-toggle="tab">Контакт</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Задача</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="padding-top: 20px">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label>Тип действия</label>
                                    <select name="Row[1]" data-role="mask_1" class="form-control">
                                        <?php
                                        foreach (\backend\models\RowValues::find()->where(['input_id'=>1])->all() as $row){
                                            ?>
                                            <option value="<?=$row['value']?>"><?=$row['label']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Тип недвижимостм</label>
                                    <select name="Row[2]" data-role="mask_2" class="form-control">
                                        <?php
                                        foreach (\backend\models\RowValues::find()->where(['input_id'=>2])->all() as $row){
                                            ?>
                                            <option value="<?=$row['value']?>"><?=$row['label']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label>Тип объекта</label>
                                    <select name="Row[3]" data-role="mask_3" class="form-control">
                                        <?php
                                        foreach (\backend\models\RowValues::find()->where(['input_id'=>3])->all() as $row){
                                            ?>
                                            <option value="<?=$row['value']?>"><?=$row['label']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Регион</label>
                                    <select name="Items[region]" class="form-control" data-role="country" onchange="ajax.loadCity($(this).val());">
                                        <option value="">--</option>
                                        <?php
                                        foreach (\common\models\Regions::find()->where(['country_id'=>2])->orderBy('title_ru')->all() as $row){
                                            ?>
                                            <option value="<?=$row['region_id']?>"><?=$row['title_ru']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label>Город</label>
                                    <select name="Items[city]" class="form-control" data-role="city" onchange="ajax.loadArea($(this).val());">

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label>Район города</label>
                                    <select name="Items[area]" class="form-control" data-role="area">

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label>Улица</label>
                                    <input type="text" name="Row[4]" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label>Номер дома</label>
                                    <input type="text" name="Row[36]" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row clearfix">
                                <div class="col-md-6">
                                    <label>Где показывать?</label>
                                    <select name="show" class="form-control">
                                        <option value="1">Только в админке</option>
                                        <option value="0">Всюду</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Фото</label>
                                    <div class="clearfix">
                                        <span class="boxImg"><img src="/admin/img/add_photo-512.png" onclick="$(this).next().click();" class="img-lg img-thumbnail" style="margin: 0 10px 10px 0">
                                        <input type="file" class="hide" multiple id="addImageToItem"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="mresult"><a href="javascript:void(0);" onclick="item.more_add(0)">Показать больше характеристик</a></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="contact">
                        <div class="form-group">
                            <label>Поиск контакта</label>
                            <input type="text" name="Search[phone]" onkeyup="ajax.searchContact($(this).val());" autocomplete="false" class="form-control">
                        </div>

                        <div id="result-search-contact"></div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <div class="form-group">
                            <label>Тип</label>
                            <select name="Tasks[type]" class="form-control">
                                <option value="">--</option>
                                <?php
                                $types = \backend\models\TaskType::find()->all();
                                foreach ($types as $type){
                                    ?>
                                    <option value="<?=$type['id']?>"><?=$type['name']?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Дата</label>
                            <input type="text" name="Tasks[data]" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Статус</label>
                            <select name="Tasks[status]" class="form-control">
                                <option value="">--</option>
                                <?php
                                $types = \backend\models\TaskStatus::find()->all();
                                foreach ($types as $type){
                                    ?>
                                    <option value="<?=$type['id']?>"><?=$type['name']?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Ответственный</label>
                            <select name="Tasks[persona]" class="form-control">
                                <option value="">--</option>
                                <?php
                                $types = \backend\models\User::find()->all();
                                foreach ($types as $type){
                                    ?>
                                    <option value="<?=$type['id']?>"><?=$type['username']?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="clearfix text-right">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Отмена</button>
                    <button type="submit" class="btn btn-primary">Сохранить</button>
                </div>
                <?php
                \yii\bootstrap\ActiveForm::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="addContact" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Добавить контакт</h4>
            </div>

            <div class="modal-body">
                <?php
                \yii\widgets\Pjax::begin(['enablePushState' => false]);
                require $_SERVER['DOCUMENT_ROOT'].'/backend/views/site/_contacts/add.php';
                \yii\widgets\Pjax::end();
                ?>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="editContact" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Редактировать контакт</h4>
            </div>

            <div class="modal-body" id="boxFromContent">

            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
