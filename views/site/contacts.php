<?php
$this->title=$title;
if (!isset($_REQUEST['sort'])) {
    $cookies = Yii::$app->response->cookies;
    $cookies->remove('statusUrl');
}

$lang=\backend\models\Help::Lang();
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->title ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= Yii::$app->homeUrl ?>"><?= $lang->index ?></a>
            </li>
            <li class="active">
                <strong><?= $this->title ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title clearfix">
                    <div class="row pull-left col-md-6">
	                    <div class="col-md-3"><a href="#addContact" data-toggle="modal" class="btn btn-info pull-left"><?= $lang->add_button ?></a></div>
	                    <div class="col-md-9">
		                    <form method="get" action="<?= Yii::$app->request->getUrl() ?>">
			                    <div class="col-md-9"><input type="text" name="q" placeholder="<?= $lang->search ?>" class="form-control"></div>
			                    <div class="col-md-3"><button  class="btn btn-success"><i class="fa fa-search"></i></button></div>
		                    </form>
	                    </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>ФИО</th>
                                <th>Телефон</th>
                                <th>Email</th>
                                <th>Активность</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($items as $item){
                                $phone = \backend\models\CrmContactContacts::find()->where(['type'=>1, 'contact'=>$item['id']])->orderBy('id')->one();
                                $email = \backend\models\CrmContactContacts::find()->where(['type'=>2, 'contact'=>$item['id']])->orderBy('id')->one();
                            ?>
                            <tr class="gradeX">
                                <td><?=$item['id']?></td>
                                <td><?=$item['fio']?></td>
                                <td><?=$phone['value']?></td>
                                <td><?=$email['value']?></td>
                                <td>--</td>
                                <td style="width: 100px">
                                    <a class="btn btn-info" onclick="contact.edit(<?=$item['id']?>);"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger" data-toggle="delete" data-id="<?= $item['id'] ?>" data-table="<?= $table ?>"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>ФИО</th>
                                <th>Телефон</th>
                                <th>Email</th>
                                <th>Активность</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?= \backend\widget\Nav::widget(['table'=>$table, 'active'=>Yii::$app->request->get('page')?Yii::$app->request->get('page'):1]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
