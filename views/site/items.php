<?php
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
$this->title=$title;
if (!isset($_REQUEST['sort'])) {
    $cookies = Yii::$app->response->cookies;
    $cookies->remove('statusUrl');
}

$lang=\backend\models\Help::Lang();
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->title ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= Yii::$app->homeUrl ?>"><?= $lang->index ?></a>
            </li>
            <li class="active">
                <strong><?= $this->title ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title clearfix">
                    <div class="row pull-left col-md-6">
	                    <div class="col-md-3"><a href="#add-item" data-toggle="modal" class="btn btn-info pull-left"><?= $lang->add_button ?></a></div>
	                    <div class="col-md-9">
		                    <form method="get" action="<?= Yii::$app->request->getUrl() ?>">
			                    <div class="col-md-9"><input type="text" name="q" value="<?=Yii::$app->request->get('q')?>" placeholder="<?= $lang->search ?>" class="form-control"></div>
			                    <div class="col-md-3"><button  class="btn btn-success"><i class="fa fa-search"></i></button></div>
		                    </form>
	                    </div>
                    </div>
                    <div class="btn-group pull-right" style="margin-left: 20px;">
                        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle">Состояние: <?=isset($_GET['status'])?\backend\models\ItemStatus::findOne($_GET['status'])['name']:'Все';?> <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" onclick="action.filter('status', '');">Все</a></li>
                            <?php
                            foreach (\backend\models\ItemStatus::find()->all() as $source){
                                ?>
                                <li><a href="javascript:void(0);" onclick="action.filter('status', <?=$source['id']?>);"><?=$source['name']?></a></li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="btn-group pull-right" style="margin-left: 20px;">
                        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle">Метка: <?=isset($_GET['label'])?\backend\models\Labels::findOne($_GET['label'])['name']:'Все';?> <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" onclick="action.filter('label', '');">Все</a></li>
                            <?php
                            foreach (\backend\models\Labels::find()->all() as $source){
                                ?>
                                <li><a href="javascript:void(0);" onclick="action.filter('label', <?=$source['id']?>);"><?=$source['name']?></a></li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="btn-group pull-right" style="margin-left: 20px;">
                        <button data-toggle="dropdown" class="btn btn-info dropdown-toggle">Источник: <?=isset($_GET['source'])?\backend\models\ItemSource::findOne($_GET['source'])['name']:'Все';?> <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" onclick="action.filter('source', '');">Все</a></li>
                            <?php
                            foreach (\backend\models\ItemSource::find()->all() as $source){
                            ?>
                            <li><a href="javascript:void(0);" onclick="action.filter('source', <?=$source['id']?>);"><?=$source['name']?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <?php
                        $dataProvider = new ActiveDataProvider([
                            'query' => $items,
                            'pagination' => [
                                'pageSize' => 20,
                            ],
                        ]);
                        ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'tableOptions' => [
                                'class' => 'table table-cursor table-striped table-bordered table-hover'
                            ],
                            'rowOptions' => function($model){
                                return ['onclick' => 'item.edit('.$model['id'].');'];
                            },
                            'columns' => [
                                'id',
                                [
                                    'attribute'=>'count_room',
                                    'label'=>'К-во комнат',
                                    'content'=>function($data){
                                        return \frontend\models\Items::getPropLabel($data['id'], '_room_');
                                    }
                                ],
                                [
                                    'attribute'=>'address',
                                    'label'=>'Адрес',
                                    'content'=>function($data){
                                        $address = '';
                                        $address.=\common\models\Areas::findOne($data['area'])['title_ru'].', ';
                                        $address.=\frontend\models\Items::getPropLabel($data['id'], '_name_').', '.\frontend\models\Items::getPropLabel($data['id'], '_home_');
                                        return $address;

                                    }
                                ],
                                [
                                    'attribute'=>'price',
                                    'label'=>'Цена',
                                    'content'=>function($data){
                                        return \frontend\models\Items::getPropLabel($data['id'], '_price_');
                                    }
                                ],
                                [
                                    'attribute'=>'name',
                                    'label'=>'Имя',
                                    'content'=>function($data){
                                        return \frontend\models\Items::getPropLabel($data['id'], '_username_');
                                    }
                                ],
                                [
                                    'attribute'=>'phone',
                                    'label'=>'Телефон',
                                    'content'=>function($data){
                                        return \frontend\models\Items::getPropLabel($data['id'], '_phone_');
                                    }
                                ],
                                [
                                    'attribute'=>'source',
                                    'label'=>'Источник',
                                    'content'=>function($data){
                                        return \backend\models\ItemSource::findOne($data['source'])['name'];
                                    }
                                ],
                                [
                                    'attribute'=>'status',
                                    'label'=>'Состояние',
                                    'filter' => \yii\helpers\ArrayHelper::map(\backend\models\ItemStatus::find()->all(), 'id', 'name'),
                                    'content'=>function($data){
                                        return \backend\models\ItemStatus::findOne($data['status'])['name'];
                                    }
                                ]
                            ],
                        ]); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>