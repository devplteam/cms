<?php
use keygenqt\autocompleteAjax\AutocompleteAjax;
$form = \yii\bootstrap\ActiveForm::begin(['action'=>'/admin/site/save-contact', 'options'=>['data-pjax' => true]]);
$model = new \backend\models\CrmContacts();
?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#contact_home" aria-controls="home" role="tab" data-toggle="tab">Информация</a></li>
        <li role="presentation"><a href="#contact_contact" aria-controls="profile" role="tab" data-toggle="tab">Контакты</a></li>
        <li role="presentation"><a href="#contact_profile" aria-controls="profile" role="tab" data-toggle="tab">Объекты</a></li>
        <li role="presentation"><a href="#contact_messages" aria-controls="messages" role="tab" data-toggle="tab">Заявки</a></li>
        <li role="presentation"><a href="#contact_settings" aria-controls="settings" role="tab" data-toggle="tab">История</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content" style="padding-top: 20px">
        <div role="tabpanel" class="tab-pane active" id="contact_home">
            <div class="form-group">
                <label>ФИО</label>
                <input type="text" name="CrmContacts[fio]" class="form-control">
            </div>
            <?= $form->field($model, 'user_site')->widget(AutocompleteAjax::classname(), [
                'multiple' => false,
                'url' => ['helper/search-user'],
                'options' => ['placeholder' => 'Аккаунт на сайте']
            ]) ?>
            <div class="form-group">
                <label>Пол</label>
                <div><input type="radio" value="0" name="CrmContacts[sex]"> Мужской</div>
                <div><input type="radio" value="1" name="CrmContacts[sex]"> Женский</div>
            </div>
            <div class="form-group">
                <label>Город</label>
                <input type="text" name="CrmContacts[city]" class="form-control">
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="contact_contact">
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <select name="ContactContacts[type][]" class="form-control">
                        <?php
                        foreach (\backend\models\CrmContactTypes::find()->all() as $type){
                        ?>
                        <option value="<?=$type['id']?>"><?=$type['name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-7">
                    <input name="ContactContacts[value][]" type="text" class="form-control">
                </div>
                <div class="col-md-1">
                    <button class="btn btn-info" type="button" onclick="contact.clone($(this));"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="contact_profile">
            <p>Недоступно при создании</p>
        </div>
        <div role="tabpanel" class="tab-pane" id="contact_messages">
            <p>Недоступно при создании</p>
        </div>
        <div role="tabpanel" class="tab-pane" id="contact_settings">
            <p>Недоступно при создании</p>
        </div>
    </div>

    <div class="clearfix text-right">
        <button type="button" class="btn btn-white" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?php
\yii\bootstrap\ActiveForm::end();