<?php
$form = \yii\bootstrap\ActiveForm::begin(['action'=>'/admin/site/save-edit-contact', 'options'=>['data-pjax' => true]]);
?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home_edit" aria-controls="home" role="tab" data-toggle="tab">Информация</a></li>
        <li role="presentation"><a href="#contact_edit" aria-controls="profile" role="tab" data-toggle="tab">Контакты</a></li>
        <li role="presentation"><a href="#profile_edit" aria-controls="profile" role="tab" data-toggle="tab">Объекты</a></li>
        <li role="presentation"><a href="#messages_edit" aria-controls="messages" role="tab" data-toggle="tab">Заявки</a></li>
        <li role="presentation"><a href="#task" aria-controls="messages" role="tab" data-toggle="tab">Задачи</a></li>
        <li role="presentation"><a href="#settings_edit" aria-controls="settings" role="tab" data-toggle="tab">История</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content" style="padding-top: 20px">
        <div role="tabpanel" class="tab-pane active" id="home_edit">
            <div class="form-group">
                <label>ФИО</label>
                <input type="text" name="CrmContacts[fio]" value="<?=$contact['fio']?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Аккаунт на сайте</label>
                <select name="CrmContacts[user_site]" class="form-control">
                    <option value="0">не привязывать</option>
                    <?php
                    $users = \common\models\User::find()->all();
                    foreach ($users as $user){
                        $class = $user['id']==$contact['user_site']?'selected':'';
                        ?>
                        <option <?=$class?> value="<?=$user['id']?>"><?=$user['username']?> (<?=$user['name']?>)</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label>Пол</label>
                <div><input type="radio" value="0" <?php if ($contact['sex']==0) echo 'checked'; ?> name="CrmContacts[sex]"> Мужской</div>
                <div><input type="radio" value="1" <?php if ($contact['sex']==1) echo 'checked'; ?> name="CrmContacts[sex]"> Женский</div>
            </div>
            <div class="form-group">
                <label>Город</label>
                <input type="text" name="CrmContacts[city]" value="<?=$contact['city']?>" class="form-control">
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="contact_edit">
            <?php
            $k=1;
            foreach ($contacts as $contact){
            ?>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <select name="ContactContacts[type][]" class="form-control">
                        <?php
                        foreach (\backend\models\CrmContactTypes::find()->all() as $type){
                            $class = $contact['type']==$type['id']?'selected':'';
                        ?>
                        <option <?=$class?> value="<?=$type['id']?>"><?=$type['name']?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-7">
                    <input name="ContactContacts[value][]" type="text" class="form-control" value="<?=$contact['value']?>">
                </div>
                <div class="col-md-1">
                    <?php
                    if ($k==count($contacts)){
                        ?>
                        <button class="btn btn-info" type="button" onclick="contact.clone($(this));"><i class="fa fa-plus"></i></button>
                        <?php
                    } else {
                    ?>
                    <button class="btn btn-info" type="button" onclick="contact.remove($(this));"><i class="fa fa-minus"></i></button>
                    <?php } ?>
                </div>
            </div>
            <?php $k++; }
            if (count($contacts)<=0){
                ?>
                <div class="form-group clearfix">
                    <div class="col-md-4">
                        <select name="ContactContacts[type][]" class="form-control">
                            <?php
                            foreach (\backend\models\CrmContactTypes::find()->all() as $type){
                                ?>
                                <option value="<?=$type['id']?>"><?=$type['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-7">
                        <input name="ContactContacts[value][]" type="text" class="form-control">
                    </div>
                    <div class="col-md-1">
                        <button class="btn btn-info" type="button" onclick="contact.clone($(this));"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile_edit">
            <?php
            $items = \backend\models\Items::find()->where(['crm_contact'=>$id])->all();
            foreach ($items as $item){
                $type = \backend\models\ItemProp::findOne(['item_id'=>$item['id'], 'row_id'=>1, 'value'=>1]);
                if (!empty($type)){
            ?>
            <p><a style="border-bottom: 1px dashed red" href="javascript:void(0);" onclick="item.edit(<?=$item['id']?>);">Объект №<?=$item['id']?></a></p>
            <?php } } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages_edit">
            <?php
            $items = \backend\models\Items::find()->where(['crm_contact'=>$id])->all();
            foreach ($items as $item){
                $type = \backend\models\ItemProp::findOne(['item_id'=>$item['id'], 'row_id'=>1, 'value'=>2]);
                if (!empty($type)){
                    ?>
                    <p><a style="border-bottom: 1px dashed red" href="javascript:void(0);" onclick="item.edit(<?=$item['id']?>);">Объект №<?=$item['id']?></a></p>
                <?php } } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="task">
            <?php
            $tasks = \backend\models\Tasks::find()->where(['user'=>$id])->all();
            foreach ($tasks as $task){
            ?>
            <div class="border-bottom" style="padding-bottom: 10px;margin-bottom: 10px;">
                <a href="/admin/tasks/edit/<?=$task['id']?>" class="product-name"><?=\backend\models\TaskType::findOne($task['type'])['name']?>, <?=date("d/m/Y", strtotime($task['data']))?></a>
                <div class="small m-t-xs"><strong>Состояние: </strong><?=\backend\models\TaskStatus::findOne($task['status'])['name']?></div>
                <div class="small m-t-xs"><strong>Ответственный: </strong><?=\backend\models\User::findOne($task['persona'])['username']?></div>
            </div>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="settings_edit">
            <div class="ibox-content" style="margin-bottom: 20px;">
                <div>
                    <div class="chat-activity-list" id="historyChat">
                        <?php
                        $histories = \backend\models\CrmHistories::find()->where(['contact'=>$id])->all();
                        foreach ($histories as $history){
                            ?>
                            <div class="chat-element">
                                <div class="media-body ">
                                    <strong><?=$history['login']?></strong>
                                    <p class="m-b-xs"><?=$history['text']?></p>
                                    <small class="text-muted"><?=date("H:i d/m/Y", strtotime($history['data']))?></small>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="chat-form">
                    <div id="addHistoryLine">
                        <input type="hidden" value="<?=$id?>" name="contact">
                        <div class="form-group">
                            <textarea name="text" class="form-control" placeholder="Замечание"></textarea>
                        </div>
                        <div class="text-right">
                            <button type="button" id="btnAddHistory" class="btn btn-sm btn-primary m-t-n-xs"><strong>Добавить</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix text-right">
        <button type="button" class="btn btn-white" data-dismiss="modal">Отмена</button>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?php
\yii\bootstrap\ActiveForm::end();