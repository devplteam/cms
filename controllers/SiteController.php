<?php
namespace backend\controllers;

use backend\models\BlockInput;
use backend\models\Contacts;
use backend\models\CrmContactContacts;
use backend\models\CrmContacts;
use backend\models\CrmHistories;
use backend\models\GeoArea;
use backend\models\GeoCity;
use backend\models\GeoRegion;
use backend\models\ItemProp;
use backend\models\Items;
use backend\models\ItemStatus;
use backend\models\Labels;
use backend\models\PageBlocks;
use backend\models\Rows;
use backend\models\RowValues;
use backend\models\Setting;
use backend\models\Tasks;
use backend\models\User;
use common\models\Areas;
use common\models\Cities;
use common\models\Orders;
use Yii;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\widgets\Pjax;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function beforeAction($action)
    {
        // ...set `$this->enableCsrfValidation` here based on some conditions...
        // call parent method that will check CSRF if such property is true.
        if ($action->id === 'delete-order') {
            # code...
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'delete', 'active', 'deactive', 'archive', 'generate-box', 'ledit', 'delete-order', 'setting', 'items', 'edit-item', 'more-fields', 'more-fields-add', 'save-item-prop', 'contacts', 'save-contact', 'edit-contact', 'load-city', 'load-area', 'search-contact', 'add-history', 'save-items', 'statements'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='main';
        return $this->render('index', [
            'orders'=>Orders::find()->all()
        ]);
    }

    public function actionSetting()
    {
        $pages=Setting::find()->all();
        if (Yii::$app->request->post()){
            foreach ($pages as $page){
                if (isset($_POST[$page['pref']])){
                    $model=Setting::find()->where(['pref'=>$page['pref']])->one();
                    $model->active=1;
                    $model->save();
                } else {
                    $model=Setting::find()->where(['pref'=>$page['pref']])->one();
                    $model->active=0;
                    $model->save();
                }
            }

            return Yii::$app->response->redirect(Yii::$app->request->referrer);
        }
        return $this->render('setting', [
            'pages'=>$pages
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {

//		$user = new User();
//		$user->email = 'admin@web4u.in.ua';
//		$user->setPassword('root');
//		$user->generateAuthKey();
//		$user->username='admin';
//		$user->status=10;
//		$user->role=0;
//		$user->save();
		
        $this->layout='login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \backend\models\LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDelete(){
        foreach ($_POST['id'] as $q){
            $class='\backend\models\\'.ucfirst($_POST['table']);
            $model=$class::findOne($q);
            $model->delete();
        }
    }

    public function actionActive(){
        foreach ($_POST['id'] as $q){
            $class='\backend\models\\'.ucfirst($_POST['table']);
            $model=$class::findOne($q);
            $model->status=1;
            $model->save();
        }
    }

    public function actionDeactive(){
        foreach ($_POST['id'] as $q){
            $class='\backend\models\\'.ucfirst($_POST['table']);
            $model=$class::findOne($q);
            $model->status=0;
            $model->save();
        }
    }

    public function actionArchive(){
//        foreach ($_POST['id'] as $q){
//            $class='\backend\models\\'.ucfirst($_POST['table']);
//            $model=$class::findOne($q);
//            $model->status=0;
//            $model->save();
//        }
    }

    public function actionGenerateBox(){
        $error='';
        $model=new PageBlocks();
        $model->name=$_POST['name'];
        $model->code=$_POST['code'];
        $model->page_id=$_POST['page_id'];
        if ($model->save()){
            $block_id=Yii::$app->db->getLastInsertID();
            for ($i=0;$i<count($_POST['input_name']);$i++){
                $input=new BlockInput();
                $input->name=$_POST['input_name'][$i];
                $input->pref=$_POST['input_pref'][$i];
                $input->block_id=$block_id;
                $input->type=$_POST['input_type'][$i];
                if (!$input->save()){
                    $error.=json_encode($input->getErrors());
                    return $error;
                }
            }
        } else {
            $error.=json_encode($model->getErrors());
            return $error;
        }
    }

    public function actionLedit(){
        $table_origin=$_POST['table'];
        $table=explode('_', $_POST['table']);
        if ($table[1]==''){
            $table_real=ucfirst($table[0]);
        } else {
            $table_real='';
            foreach ($table as $tab){
                $table_real.=ucfirst($tab);
            }
        }
        $class='\backend\models\\'.$table_real;
        $model=$class::findOne($_POST['id']);
        $model->$_POST['input']=$_POST['value'];
        $model->save();
    }

    public function actionDeleteOrder(){
        $order=Orders::findOne($_POST['id']);
        $order->delete();
        return Yii::$app->response->redirect('/admin/');
    }

    public function actionItems(){
        //$offet = Yii::$app->request->get('page')?(Yii::$app->request->get('page')-1)*20:0;
        $where = ['AND'];
        if (Yii::$app->request->get('q')){
            $ids = [];
            $items = Yii::$app->db->createCommand("select item_id from item_prop where `value` like '%".trim(Yii::$app->request->get('q'))."%'")->queryAll();
            foreach ($items as $item){
                $ids[] = $item['item_id'];
            }
            $where[] = ['IN', 'id', $ids];
        }

        if (Yii::$app->request->get('label')){
            $where[] = ['label'=>Yii::$app->request->get('label')];
        }

        if (Yii::$app->request->get('status')){
            $where[] = ['status'=>Yii::$app->request->get('status')];
        }

        if (Yii::$app->request->get('source')){
            $where[] = ['source'=>Yii::$app->request->get('source')];
        }
        $items = Items::find()->where($where);

        $items->join('LEFT JOIN', 'item_prop', 'item_prop.item_id = items.id')->where(['AND', ['row_id'=>1], ['IN', 'value', [1,3]]]);

        $items->orderBy('id desc');
        
        return $this->render('items', [
            'title' => 'База недвижимости',
            'items' => $items,
            'table' => 'items'
        ]);
    }

    public function actionStatements(){
        //$offet = Yii::$app->request->get('page')?(Yii::$app->request->get('page')-1)*20:0;
        $where = ['AND'];
        if (Yii::$app->request->get('q')){
            $ids = [];
            $items = Yii::$app->db->createCommand("select item_id from item_prop where `value` like '%".trim(Yii::$app->request->get('q'))."%'")->queryAll();
            foreach ($items as $item){
                $ids[] = $item['item_id'];
            }
            $where[] = ['IN', 'id', $ids];
        }

        if (Yii::$app->request->get('label')){
            $where[] = ['label'=>Yii::$app->request->get('label')];
        }

        if (Yii::$app->request->get('status')){
            $where[] = ['status'=>Yii::$app->request->get('status')];
        }

        if (Yii::$app->request->get('source')){
            $where[] = ['source'=>Yii::$app->request->get('source')];
        }
        $items = Items::find()->where($where);

        $items->join('LEFT JOIN', 'item_prop', 'item_prop.item_id = items.id')->where(['AND', ['row_id'=>1], ['IN', 'value', [2,4]]]);

        $items->orderBy('id desc');

        return $this->render('items', [
            'title' => 'База недвижимости',
            'items' => $items,
            'table' => 'items'
        ]);
    }

    public function actionEditItem(){
        $item = Items::findOne(Yii::$app->request->post('id'));
        $html = '<input type="hidden" value="'.Yii::$app->request->post('id').'" name="id">
        <div class="row clearfix">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Состояние</label>
                    <select name="status" class="form-control">';
                        foreach (ItemStatus::find()->all() as $status){
                            $html.='<option '.($status['id']==$item['status']?'selected':'').' value="'.$status['id'].'">'.$status['name'].'</option>';
                        }

                    $html.='</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Метка</label>
                    <select name="label" class="form-control">';
                        foreach (Labels::find()->all() as $label){
                            $html.='<option '.($label['id']==$item['label']?'selected':'').' value="'.$label['id'].'">'.$label['name'].'</option>';
                        }
                    $html.='</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Дата</label>
                    <input type="text" readonly name="date_create" value="'.$item['date_create'].'" class="form-control">
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Регион</label>
                    <select name="region" class="form-control">';
                        foreach (GeoRegion::find()->all() as $status){
                            $html.='<option '.($status['id']==$item['region']?'selected':'').' value="'.$status['id'].'">'.$status['name'].'</option>';
                        }
                    $html.='</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Город</label>
                    <select name="city" class="form-control">';
                        foreach (GeoCity::find()->where(['region'=>$item['region']])->all() as $label){
                            $html.='<option '.($label['id']==$item['city']?'selected':'').' value="'.$label['id'].'">'.$label['name'].'</option>';
                        }
                    $html.='</select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Район города</label>
                    <select name="area" class="form-control">
                        <option value="0">--</option>';
                        foreach (GeoArea::find()->where(['city'=>$item['city']])->all() as $label){
                            $html.='<option '.($label['id']==$item['area']?'selected':'').' value="'.$label['id'].'">'.$label['name'].'</option>';
                        }
                    $html.='</select>
                </div>
            </div>
        </div>
        <div class="row">';
            if ($item['source']!=1){
            $html.='<div class="col-md-9">
                <div class="form-group">
                    <label>Ссылка на источник</label>
                    <section><a href="'.$item['link'].'" target="_blank">'.$item['link'].'</a></section>
                </div>
            </div>';
            }

            $html.='<div class="col-md-4">
                <div class="form-group">
                    <label>Показывать на главной</label>
                    <select class="form-control" name="show_index">
                        <option value="0">Нет</option>
                        <option value="1">Да</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Где показывать?</label>
                    <select name="show" class="form-control">';
                        if ($item['show']==0){
                            $html.='<option value="0" selected>На сайте</option>';
                        } else {
                            $html.='<option value="0">На сайте</option>';
                        }

                        if ($item['show']==1){
                            $html.='<option value="1" selected>Только в админке</option>';
                        } else {
                            $html.='<option value="1">Только в админке</option>';
                        }
        $html.='</select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Заметка</label>
                    <textarea class="form-control">'.$item['comment'].'</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Фото</label>
                    <div class="clearfix">';
                        $pics = ItemProp::findOne(['row_id'=>29, 'item_id'=>$item['id']]);
                        if (!empty($pics)){
                            foreach (json_decode($pics['value']) as $pic){
                                $html.='<span class="boxImg"><img src="'.$pic.'" class="img-lg img-thumbnail" style="margin: 0 10px 10px 0"><i class="fa fa-times"></i><input type="hidden" value="'.$pic.'" name="Row[29][]"></span>';
                            }
                        }
        $html.='<span class="boxImg"><img src="/admin/img/add_photo-512.png" onclick="$(this).next().click();" class="img-lg img-thumbnail" style="margin: 0 10px 10px 0"><input type="file" class="hide" id="addImageToItem"></span>';
                    $html.='</div>
                </div>
            </div>
        </div>
        <div id="mresult"><a href="javascript:void(0);" onclick="item.more('.$item['id'].')">Показать больше характеристик</a></div>';

        return $html;
    }
    
    public function actionMoreFields(){
        $html = '';
        $item['id'] = Yii::$app->request->post('id');
        foreach (Rows::find()->where(['not in', 'id', [32,33]])->andWhere(['IN', 'show', [0,2]])->orderBy('type')->all() as $row){
            $valueItem = ItemProp::findOne(['item_id'=>$item['id'], 'row_id'=>$row['id']])['value'];
            switch ($row['type']){
                case '1':
                    $html.='<div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-right">
                                    <label>'.$row['name'].'</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="Row['.$row['id'].']" type="text" value="'.$valueItem.'" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>';
                    break;

                case '8':
                    $html.='<div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-right">
                                    <label>'.$row['name'].'</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="Row['.$row['id'].']" type="number" value="'.$valueItem.'" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>';
                    break;

                case '4':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">
                                <select name="Row['.$row['id'].']" class="form-control">
                                    <option value="">--</option>';
                    foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                        $html.='<option '.($value==$valueItem?'selected':'').' value="'.$value['value'].'">'.$value['label'].'</option>';
                    }
                    $html.='</select>
                            </div>
                        </div>
                    </div>';
                    break;

                case '2':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">';
                    foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                        $html.='<label style="margin: 0 10px 10px 0"><input '.($value['value']==$valueItem?'checked':'').' type="radio" name="Row['.$row['id'].']" value="'.$value['value'].'"> '.$value['label'].'</label>';
                    }
                    $html.='</div>
                        </div>
                    </div>';
                    break;

                case '3':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">';
                    $valueItem = json_decode($valueItem);
                    if (count($valueItem)<=0){
                        $valueItem = [];
                    }
                    foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                        $html.='<label style="margin: 0 10px 10px 0"><input type="checkbox" '.(in_array($value['value'], $valueItem)?'checked':'').' name="Row['.$row['id'].']" value="'.$value['value'].'"> '.$value['label'].'</label>';
                    }
                    $html.='</div>
                        </div>
                    </div>';
                    break;

                case '7':
                    $html.='<div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="Row['.$row['id'].']" class="form-control" rows="5">'.$valueItem.'</textarea>
                            </div>
                        </div>
                    </div>';
                    break;
            }
        }
        $html.='<div class="clearfix"></div>';

        return $html;
    }

    public function actionMoreFieldsAdd(){
        $html = '';
        $rows = Yii::$app->db->createCommand("select * from `rows` where step_id in (select id from steps where mask='".Yii::$app->request->post('mask')."') and id not in (1,2,3) and show in (0,2)")->queryAll();
        foreach ($rows as $row){
            switch ($row['type']){
                case '1':
                    $html.='<div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-right">
                                    <label>'.$row['name'].'</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="Row['.$row['id'].']" type="text" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>';
                    break;

                case '8':
                    $html.='<div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4 text-right">
                                    <label>'.$row['name'].'</label>
                                </div>
                                <div class="col-md-8">
                                    <input name="Row['.$row['id'].']" type="number" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>';
                    break;

                case '4':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">
                                <select name="Row['.$row['id'].']" class="form-control">
                                    <option value="">--</option>';
                                    foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                                        $html.='<option value="'.$value['value'].'">'.$value['label'].'</option>';
                                    }
                                $html.='</select>
                            </div>
                        </div>
                    </div>';
                    break;

                case '2':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">';
                                foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                                    $html.='<label style="margin: 0 10px 10px 0"><input type="radio" name="Row['.$row['id'].']" value="'.$value['value'].'"> '.$value['label'].'</label>';
                                }
                            $html.='</div>
                        </div>
                    </div>';
                    break;

                case '3':
                    $html.='<div class="clearfix"></div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">';
                                foreach (RowValues::find()->where(['input_id'=>$row['id']])->all() as $value){
                                    $html.='<label style="margin: 0 10px 10px 0"><input type="checkbox" name="Row['.$row['id'].']" value="'.$value['value'].'"> '.$value['label'].'</label>';
                                }
                            $html.='</div>
                        </div>
                    </div>';
                    break;

                case '7':
                    $html.='<div class="form-group">
                        <div class="row">
                            <div class="col-md-3 text-right">
                                <label>'.$row['name'].'</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="Row['.$row['id'].']" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>';
                    break;
            }
        }
        $html.='<div class="clearfix"></div>';
        return $html;
    }

    public function actionSaveItemProp(){
        $model = Items::findOne(Yii::$app->request->post('id'));
        $model->status = Yii::$app->request->post('status');
        $model->label = Yii::$app->request->post('label');
        $model->region = Yii::$app->request->post('region');
        $model->city = Yii::$app->request->post('city');
        $model->area = Yii::$app->request->post('area');
        $model->show_index = Yii::$app->request->post('show_index');
        $model->show = Yii::$app->request->post('show');
        if ($model->save()){
            if (Yii::$app->request->post('Row')) {
                ItemProp::deleteAll(['item_id' => Yii::$app->request->post('id')]);
                foreach (Yii::$app->request->post('Row') as $key => $value) {
                    if (is_array($value)){
                        $value = json_encode($value);
                    }
                    if (trim($value) != '') {
                        $new = new ItemProp();
                        $new->item_id = Yii::$app->request->post('id');
                        $new->row_id = $key;
                        $new->value = $value;
                        $new->value_int = (int)$value;
                        $new->save();
                    }
                }
            }

            return 'success';
        } else {
            return 'error';
        }
    }

    public function actionContacts(){
        $where = ['OR'];
        if (Yii::$app->request->get('q')){
            $where[] = ['like', 'fio', Yii::$app->request->get('q')];
            $where[] = ['like', 'city', Yii::$app->request->get('q')];
            $ids = [];
            $conts = CrmContactContacts::find()->where(['like', 'value', Yii::$app->request->get('q')])->all();
            foreach ($conts as $cont){
                $ids[] = $cont['contact'];
            }
            $where[] = ['IN', 'id', $ids];
        }
        $contacts = CrmContacts::find()->where($where)->all();
        return $this->render('contacts', [
            'items' => $contacts,
            'title' => 'Контакты',
            'table' => 'crm_contacts'
        ]);
    }
    public function actionSaveContact(){
        $model = new CrmContacts();
        if ($model->load(Yii::$app->request->post())){
            $model->create = date("Y-m-d H:i:s");
            if ($model->save()){
                $last = Yii::$app->db->getLastInsertID();
                for ($i=0; $i<count(Yii::$app->request->post('ContactContacts')['type']); $i++){
                   $contact = new CrmContactContacts();
                   $contact->contact = $last;
                   $contact->type = Yii::$app->request->post('ContactContacts')['type'][$i];
                   $contact->value = Yii::$app->request->post('ContactContacts')['value'][$i];
                   $contact->save();
                }
                return 'Контакт создан';
            } else {
                return print_r($model->getErrors());
            }
        } else {
            return print_r($model->getErrors());
        }
    }

    public function actionEditContact(){
        $contact = CrmContacts::findOne(Yii::$app->request->post('id'));
        $contacts = CrmContactContacts::find()->where(['contact'=>Yii::$app->request->post('id')])->all();
        return $this->renderPartial('_contacts/edit', [
            'contact' => $contact,
            'contacts' => $contacts,
            'id' => Yii::$app->request->post('id')
        ]);
    }

    public function actionLoadCity(){
        $html = '<option value="">--</option>';
        foreach (Cities::find()->where(['region_id'=>Yii::$app->request->post('id')])->orderBy('title_ru')->all() as $city){
            $html.='<option value="'.$city['city_id'].'">'.$city['title_ru'].'</option>';
        }

        return $html;
    }

    public function actionLoadArea(){
        $html = '<option value="">--</option>';
        foreach (Areas::find()->where(['city_id'=>Yii::$app->request->post('id')])->orderBy('title_ru')->all() as $city){
            $html.='<option value="'.$city['id'].'">'.$city['title_ru'].'</option>';
        }

        return $html;
    }

    public function actionSearchContact(){
        $html = '';
        $res = Yii::$app->db->createCommand("select * from crm_contacts where id in (select contact from crm_contact_contacts where value like '%".Yii::$app->request->post('q')."%')")->queryAll();
        if (count($res)==0){
            $users = \common\models\User::find()->all();
            $us = '';
            foreach ($users as $user){
                $us.='<option value="'.$user['id'].'">'.$user['username'].' ('.$user['name'].')</option>';
            }
            $html = '<div class="form-group">
                <label>ФИО</label>
                <input type="text" name="CrmContacts[fio]" class="form-control">
            </div>
            <div class="form-group">
                <label>Аккаунт на сайте</label>
                <select name="CrmContacts[user_site]" class="form-control">
                    <option value="0">не привязывать</option>
                    '.$us.'
                </select>
            </div>
            <div class="form-group">
                <label>Пол</label>
                <div><input type="radio" value="0" name="CrmContacts[sex]"> Мужской</div>
                <div><input type="radio" value="1" name="CrmContacts[sex]"> Женский</div>
            </div>
            <div class="form-group">
                <label>Город</label>
                <input type="text" name="CrmContacts[city]" class="form-control">
            </div>';
        } else {
            foreach ($res as $row){
                $html.='<div class="alert alert-success">
                    <input type="radio" name="exContact" value="'.$row['id'].'">
                    <label>ФИО: </label>'.$row['fio'].', 
                    <label>Город: </label>'.$row['city'].'
                </div>';
            }
        }

        return $html;
    }

    public function actionAddHistory(){
        $model = new CrmHistories();
        $model->contact = Yii::$app->request->post('contact');
        $model->text = Yii::$app->request->post('text');
        $model->data = date("Y-m-d H:i:s");
        $model->login = Yii::$app->user->identity->username;
        if ($model->save()){
            return json_encode(['login'=>Yii::$app->user->identity->username, 'contact'=>Yii::$app->request->post('contact'), 'date'=>date("H:i d/m/Y", strtotime($model->data)), 'text'=>Yii::$app->request->post('text')]);
        }
    }

    public function actionSaveItems(){

        // Определение контакта
        if (Yii::$app->request->post('exContact')){
            $contact = CrmContacts::findOne(Yii::$app->request->post('exContact'));
        } else {
            $contact = new CrmContacts();
            $contact->fio = Yii::$app->request->post('CrmContacts')['fio'];
            $contact->user_site = Yii::$app->request->post('CrmContacts')['user_site'];
            $contact->sex = Yii::$app->request->post('CrmContacts')['sex'];
            $contact->city = Yii::$app->request->post('CrmContacts')['city'];
            $contact->create = date("Y-m-d H:i:s");
            $contact->save();
        }

        //Создаем объект
        $item = new Items();
        $item->date_create = date("Y-m-d H:i:s");
        $item->user = $contact['user_site'];
        $item->status = 1;
        $item->region = Yii::$app->request->post('Items')['region'];
        $item->city = Yii::$app->request->post('Items')['city'];
        $item->area = Yii::$app->request->post('Items')['area'];
        $item->source = 1;
        $item->link = '';
        $item->label = 6;
        $item->comment = '';
        $item->show_index = 0;
        $item->crm_contact = $contact['id'];
        $item->save();

        $last = Yii::$app->db->getLastInsertID();

        foreach (Yii::$app->request->post('Row') as $key=>$value){
            if (is_array($value)){
                $value = json_encode($value);
            }
            $prop = new ItemProp();
            $prop->item_id = $last;
            $prop->row_id = $key;
            $prop->value = $value;
            $prop->value_int = (int)$value;
            $prop->save();
        }

        // Создание задачи
        if (Yii::$app->request->post('Tasks')['type']!='') {
            $task = new Tasks();
            $task->type = Yii::$app->request->post('Tasks')['type'];
            $task->data = Yii::$app->request->post('Tasks')['data'];
            $task->status = Yii::$app->request->post('Tasks')['status'];
            $task->persona = Yii::$app->request->post('Tasks')['persona'];
            $task->user = $contact['id'];
            $task->save();
        }

        return $this->redirect(Yii::$app->request->referrer);
    }
}
