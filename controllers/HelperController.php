<?php

namespace backend\controllers;

use backend\models\Helper;
use backend\models\SignupForm;
use common\models\Products;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class HelperController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'upload') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['upload', 'file-info', 'search', 'info-product', 'search-user'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionUpload()
    {
        $img = explode(',', str_replace(' ', '+', $_POST['tmp']));
        $img= base64_decode($img[1]);
        $name=md5(date("YmdHis").$_POST['name']);
        $ext=explode('.', $_POST['name']);
        $ext=array_pop($ext);
        $fpng = fopen($_SERVER['DOCUMENT_ROOT'].'/frontend/web'.$_POST['folder'].$name.".".$ext, "w");
        fwrite($fpng,$img);
        fclose($fpng);
        return $_POST['folder'].$name.'.'.$ext;
    }

    public function actionFileInfo()
    {
        $size = filesize($_SERVER['DOCUMENT_ROOT'] . '/frontend/web' . Yii::$app->request->post('file'));
        $type = mime_content_type($_SERVER['DOCUMENT_ROOT'] . '/frontend/web' . Yii::$app->request->post('file'));
        $name = explode('/', Yii::$app->request->post('file'));
        return json_encode(['name'=>array_pop($name), 'size'=>$size, 'type'=>$type]);
    }

    public function actionSearch(){
        ?>
        <div class="m-list-search__results">
        <?php
        $q=Yii::$app->request->get('query');
        $result = Yii::$app->db->createCommand('SHOW TABLES;')->queryAll();
        $ext=['access', 'admin', 'logs', 'migration'];
        foreach ($result as $row){
            foreach ($row as $key=>$value){
                if (!in_array($value, $ext)){
                    $rows=Yii::$app->db->createCommand('SHOW COLUMNS FROM '.$value)->queryAll();
                    $like_search="";
                    foreach ($rows as $field){
                        $like_search.=$field['Field']." like '%".$q."%' or ";
                    }
                    $like_search=substr($like_search, 0, -4);
                    $sql=Yii::$app->db->createCommand('select * from '.$value.' where '.$like_search)->queryAll();
                    if (!empty($sql)){
                        $table = explode('_', $value);
                        if (empty($table[1])) {
                            $table_real = ucfirst($table[0]);
                        } else {
                            $table_real = '';
                            foreach ($table as $tab) {
                                $table_real .= ucfirst($tab);
                            }
                        }
                        if ($value == 'user') {
                            $class = '\frontend\models\\' . $table_real;
                        } else {
                            $class = '\backend\models\\' . $table_real;
                        }

                        $model = new $class();
                        ?>
                        <a href="/admin/<?php echo $value?>" class="m-list-search__result-item">
                            <span class="m-list-search__result-item-text">Знайдено в <strong><?php echo $model::$pageName?></strong></span>
                        </a>
                        <?php
                    }
                }
            }
        }
        ?>
        </div>
        <?php
    }

    public function actionInfoProduct(){
        $item = Products::findOne(Yii::$app->request->post('id'));

        return json_encode(['article'=>$item['article'], 'price'=>$item['price']]);
    }

    public function actionSearchUser($term){
        $users = User::find()->where(['OR', ['like', 'username', $term], ['LIKE', 'name', $term]])->all();
        $arrays = [];
        foreach ($users as $user){
            $arrays[] = [
                'id' => $user['id'],
                'label' => $user['username'].($user['name']!=''?' ('.$user['name'].')':'')
            ];
        }

        return json_encode($arrays);
    }
}
