<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class CrmContactContacts extends  ActiveRecord{

    public function nameTable(){
        return 'Контакты контактов';
    }

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'contact'=>'ID контакта',
            'type'=>'Тип',
            'value'=>'Значение',
        ];
    }

    public function rules()
    {
        return [
            [['contact', 'type', 'value'],'required']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'pref',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'active',
                'type'=>'checkbox',
                'display'=>false
            ]
        ];
    }

}