<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class CrmContacts extends  ActiveRecord{

    public function nameTable(){
        return 'Контакты';
    }

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'fio'=>'ФИО',
            'user_site'=>'Пользователь сайт',
            'sex'=>'Пол',
            'city'=>'Город',
            'create'=>'Создан',
        ];
    }

    public function rules()
    {
        return [
            [['fio', 'sex', 'city'],'required'],
            [['create', 'user_site'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'pref',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'active',
                'type'=>'checkbox',
                'display'=>false
            ]
        ];
    }

}