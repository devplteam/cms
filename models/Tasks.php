<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class Tasks extends  ActiveRecord{

    public function nameTable(){
        return 'Задачи';
    }

    public function attributeLabels()
    {
        return [
            'id'=>'ID',
            'type'=>'Тип задачи',
            'data'=>'Дата',
            'status'=>'Состояние',
            'persona'=>'Ответственный',
            'item'=>'Объект или заявка',
            'user'=>'Контакт',
        ];
    }

    public function rules()
    {
        return [
            [['type', 'data', 'status', 'persona'],'required'],
            [['item', 'user'], 'safe']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'type',
                'type'=>'select',
                'display'=>true,
                'table' => [
                    'name' => 'task_type',
                    'value' => 'id',
                    'text' => 'name'
                ]
            ],
            [
                'name'=>'data',
                'type'=>'datetimepicker',
                'display'=>true
            ],
            [
                'name'=>'status',
                'type'=>'select',
                'display'=>true,
                'table' => [
                    'name' => 'task_status',
                    'value' => 'id',
                    'text' => 'name'
                ]
            ],
            [
                'name'=>'persona',
                'type'=>'select',
                'display'=>true,
                'table' => [
                    'name' => 'admin',
                    'value' => 'id',
                    'text' => 'username'
                ]
            ],
            [
                'name'=>'item',
                'type'=>'url',
                'display'=>true,
                'action' => 'item.edit(***value***);'
            ],
            [
                'name'=>'user',
                'type'=>'select',
                'display'=>true,
                'table' => [
                    'name' => 'crm_contacts',
                    'value' => 'id',
                    'text' => 'fio'
                ],
                'show' => 'contact.edit(***value***);'
            ],
        ];
    }

}