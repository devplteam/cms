<?php
/**
 * Created by PhpStorm.
 * User: gebruiker
 * Date: 20.01.17
 * Time: 13:59
 */

namespace backend\models;

use yii\db\ActiveRecord;


class CrmHistories extends  ActiveRecord{

    public function nameTable(){
        return 'История';
    }

    public function attributeLabels()
    {
        return [

            'id'=>'ID',
            'contact'=>'ID контакта',
            'text'=>'Текст',
            'data'=>'Дата и время',
            'login'=>'Логин',
        ];
    }

    public function rules()
    {
        return [
            [['contact', 'text', 'data', 'login'],'required']
        ];
    }

    public function rows(){
        return [
            [
                'name'=>'id',
                'type'=>'input',
                'display'=>true,
                'attr'=>[
                    'disabled'=>'disabled'
                ]
            ],
            [
                'name'=>'name',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'pref',
                'type'=>'input',
                'display'=>true
            ],
            [
                'name'=>'active',
                'type'=>'checkbox',
                'display'=>false
            ]
        ];
    }

}