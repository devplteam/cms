(function($) {
    $.fn.load4u = function(options) {
        options = $.extend({
            path: '/source/sites/',
            onsuccess: function(res){
                console.log(res);
            },
            onerror: function (res) {
                console.log(res);
            },
            beforeload: false,
            param: null,
            box: '#boxToLoad'
        }, options);

        var th = $(this);

        $(this).change(function(evt) {
            var files = evt.target.files;
            var max_size=$(this).attr('data-maxsize');
            for (var i = 0, f; f = files[i]; i++) {
                renderImage(f, max_size);
            }
        });

        function renderImage(file, max_size) {
            var reader = new FileReader();
            reader.onload = function(event) {
                var size=(event.total/1024)/1024;
                var type=(event.target.result).split(';');
                type=type[0].split('/');
                if (size>max_size){
                    if (options.onerror!==false){
                        options.onerror();
                    }
                } else {
                    if (options.beforeload !== false) {
                        options.beforeload();
                    }
                    $(options.box).append('<div id="load4uProgress" style="position:absolute;background:rgba(0, 0, 0, .5);z-index:999;top: 0;left: 0;width: 100%;height: 100%;text-align: center;"><div style="width: 90%;position: relative;height: 20px;left: 5%;margin-top: -10px;background: white;border-radius: 4px;overflow: hidden;top: 50%"><div style="position: absolute;left: 0;top: 0;height: 100%;width: 0;background: #0a8cf0;-webkit-transition: linear 0.2s;-moz-transition: linear 0.2s;-ms-transition: linear 0.2s;-o-transition: linear 0.2s;transition: linear 0.2s;" id="progressLine4u"></div></div></div>');
                    the_url = event.target.result;
                    var param = '';
                    for (var key in options.param) {
                        param += '&' + key + '=' + options.param[key];
                    }

                    $.ajax({
                        type: 'POST',
                        url: '/admin/helper/upload',
                        data: 'tmp=' + the_url + '&name=' + file.name + param + '&folder=' + options.path,
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) {
                                myXhr.upload.addEventListener('progress', progress, false);
                            }
                            return myXhr;
                        },
                        cache: false,
                        success: function (res) {
                            options.onsuccess(res, th);
                        },
                        error: function (res) {
                            options.onerror(res);
                        }
                    });
                }
            };
            // когда файл считывается он запускает событие OnLoad.
            reader.readAsDataURL(file);
        }

        function progress(e){

            if(e.lengthComputable){
                var max = e.total;
                var current = e.loaded;

                var Percentage = (current * 100)/max;
                $("#load4uProgress #progressLine4u").css('width', Percentage+'%');
                if(Percentage >= 100)
                {
                    $("#load4uProgress").remove();
                }
            }
        }

        return this;
    };
})(jQuery);
